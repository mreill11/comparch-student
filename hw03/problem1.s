#Problem 1
	.set noreorder
	.data

	.text
	.globl main
	.ent main

main:

#int score = 84;	
	addi $s0, $0, 84  # add immediate to store 84

#int grade;
	add $s1, $0, $0   # uninitialized int

#if (score >= 90)
	slti $t0, $s0, 90 # compare registers
	bne $t0, $0, else_one # what to do if not equal
	nop               # null op

#grade = 4;
	addi $s1, $0, 4   # add immediate to set grade
	j print           # jump to print
	nop

else_one:

# everything below follows the same pattern as above
#else if (score >= 80)
	slti $t1, $s0, 80
	bne $t1, $0, else_two
	nop

#grade = 3;
	addi $s1, $0, 3
	j print
	nop

else_two:

#else if (score >= 70)
	slti $t2, $s0, 70
	bne $t2, $0, else_final
	nop

#grade = 2;
	addi $s1, $0, 2
	j print
	nop

else_final:

#else
#grade = 0;

print:
#PRINT_HEX_DEC(grade);
	add $a0, $0, $s1 # a0 = s1
	ori $v0, $0, 20  # logical or
	syscall

#EXIT;
	ori $v0, $0, 10  # logical or
	syscall

	.end main