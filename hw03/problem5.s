#Problem 5
	.set noreorder
	.data

	.text
	.globl main
	.ent main

#typedef struct elt {
# int value;
# struct elt *next;
#} elt;

main: 

# elt *head;
# v0 is the pointer to the head object

# elt *newelt;
# tv is the pointer to the newelt object

#newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8
	ori $v0, $0, 9
	syscall

#newelt->value = 1;
	addi $t0, $0, 1
	sw $t0, 0($v0)

#newelt->next = 0;
	addi $v0, $v0, 4
	addi $t1, $0, 0
	sw $t1, 0($v0)

#head = newelt;
	addi $v0, $v0, -4
	add $v1, $0, $v0

#newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8
	ori $v0, $0, 9
	syscall

#newelt->value = 2;
	addi $t2, $0, 2
	sw $t2, 0($v0)

#newelt->next = head;
	addi $v0, $v0, 4
	sw $v1, 0($v0)

#head = newelt;
	addi $v0, $v0, -4
	add $a1, $0, $v0

#PRINT_HEX_DEC(head->value);
	lw $t4, 0($a1)
	add $a0, $t4, $0
	ori $v0, $0, 20
	syscall

#PRINT_HEX_DEC(head->next->value);
	addi $a1, $a1, 4
	lw $t5, 0($a1)
	lw $t6, 0($t5)
	add $a0, $t6, $0
	ori $v0, $0, 20
	syscall

#EXIT
	ori $v0, $0, 10
	syscall

	.end main
