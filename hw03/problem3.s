#Problem 3
	.set noreorder
	.data
#int A[8]
A:.word 32
	.text
	.globl main
	.ent main

main:

#int i;
	addi $s0, $s0, 2 # i = $s0

#A[0] = 0;
	la $t0, A        # $t0 = &A
	sw $0, ($t0)

#A[1] = 1;
	addi $t0, $t0, 4 # move up one index
	addi $t1, $0, 1
	sw $t1, ($t0)
	addi $t0, $t0, 4

for_loop:

#for (i = 2; i < 8; i++) {
	slti $t2, $s0, 8
	beq $t2, $0, exit
	nop

#A[i] = A[i-1] + A[i-2];
	addi $t3, $t0, -4 # addr of i1
	addi $t4, $t0, -8 # addr of i2
	lw $t5, 0($t3)
	lw $t6, 0($t4)
	add $s1, $t5, $t6
	sw $s1, 0($t0)

#PRINT_HEX_DEC(A[i]);
	add $a0, $0, $s1
	ori $v0, $0, 20
	syscall

	addi $s0, $s0, 1
	addi $t0, $t0, 4
	j for_loop
	nop

exit:
#EXIT;
	ori $v0, $0, 10
	syscall

	.end main
