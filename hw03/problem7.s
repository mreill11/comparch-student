.set noreorder
.data

    # void print(int a) {
    print:
        ori $v0, $0, 20		# configure system call to print hex ($a0)
        syscall			    # print hex'

        jr $ra              # return
        nop
    # }

    # int sum3(int a, int b, int c) {
    sum3:

        # loads into stack
        addi $sp, $sp, -12
        sw $t0, 0($sp)
        sw $t1, 4($sp)
        sw $t2, 8($sp)

        # adds
        add $v0, $t0, $t1   # sum = a + b
        add $v0, $v0, $t2   # sum = sum + c

        # gets original values from stack
        lw $t0, 0($sp)
        lw $t1, 4($sp)
        lw $t2, 8($sp)
        addi $sp, $sp, 12

        jr $ra              # return sum
        nop
    # }

    # int polynomial(int a, int b, int c, int d, int e) {
    polynomial:

        # Save items into stack
        addi $sp, $sp, -24
        sw $s0, 0($sp)
        sw $s1, 4($sp)
        sw $s2, 8($sp)
        sw $s3, 12($sp)
        sw $s4, 16($sp)
        sw $ra, 20($sp)

        # x = a << b
        sll $t0, $s0, $s1

        # y = c << d
        sll $t1, $s2, $s3

        add $t2, $0, $s4 # hold e in $t2

        # z = sum3(x, y e)
        jal sum3
        nop
        add $t2, $0, $v0 # z is in $t2

        # print(x)
        add $a0, $0, $t0
        jal print
        nop

        # print(y)
        add $a0, $0, $t1
        jal print
        nop

        # print(z)
        add $a0, $0, $t2
        jal print
        nop

        lw $s0, 0($sp)
        lw $s1, 4($sp)
        lw $s2, 8($sp)
        lw $s3, 12($sp)
        lw $s4, 16($sp)
        lw $ra, 20($sp)
        addi $sp, $sp, 24

        # return z
        add $v0, $0, $t2
        jr $ra  # exit
        nop
    # }

.text
.globl main
.ent main

# int main() {
main:

    # int a = 2
    addi $s0, $0, 2

    addi $s1, $0, 3
    addi $s2, $0, 4
    addi $s3, $0, 5
    addi $s4, $0, 6

    # int f = polynomial(a, 3, 4, 5, 6)
    jal polynomial
    nop
    add $s1, $v0, $0 # f is held in $s1

    # print(a)
    add $a0, $0, $s0
    jal print
    nop

    # print(f)
    add $a0, $0, $s1
    jal print
    nop

    ori $v0, $0, 10     # exit
    syscall
# }

    .end main
