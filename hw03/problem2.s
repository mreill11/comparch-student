# Problem 2
	.set noreorder
	.data
# char A[] = {1, 25, 7, 9, -1}:
A:	.byte 1, 25, 7, 9, -1
	.text
	.globl main
	.ent main

main:

#int i
	# $s0 = i

#int current
	# $s1 = current

#int max
	# $s2 = max

#i  = 0
	addi $s0, $0, 0    # s0 + 0

#current = A[0]
	la $t2, A
	lb $s1, 0($t2)

#max = 0
	addi $s2, $0, 0

while_loop:

#while (current > 0)
	slti $t0, $s1, 1
	bne $t0, $0, exit
	nop

#if (current > max)
	slt $t1, $s2, $s1
	beq $t1, $0, next
	nop

#max = current
	add $s2, $0, $s1

next:

#i = i + 1
	addi $s0, $s0, 1
	addi $t2, $t2, 1

#current = A[i]
	lb $s1, 0($t2)
	j while_loop


print:
#PRINT_HEX_DEC(grade);
	add $a0, $0, $s2
	ori $v0, $0, 20
	syscall

# EXIT;
	ori $v0, $0, 10
	syscall

	.end main


