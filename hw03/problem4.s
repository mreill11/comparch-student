#Problem 4
	.set noreorder
	.data

	.text
	.globl main
	.ent main

# typedef struct record {# treated as an array
#   int field0;
#   int field1;
# } record;

main:

#record *r = (record *) MALLOC(sizeof(record));
	addi $a0, $0, 8
	ori $v0, $0, 9
	syscall

#r->field0 = 100;
	addi $t0, $0, 100
	sw $t0, 0($v0)

#r->field1 = -1;
	addi $v0, $v0, 4
	addi $t1, $0, -1
	sw $t1, 0($v0)

#EXIT
	ori $v0, $0, 10
	syscall

	.end main
